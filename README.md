# Dual Channel LED Dimmer PCB

Circuit Simulator, EasyEDA and Gerber files for a 555-based dual channel LED dimmer PCB

## Images

![Front and back of the PCB](rev_1/images/pcb.png)

## Revision 1

- [Circuit Simulator](https://www.falstad.com/circuit/circuitjs.html) file
    - [Dual Channel LED Dimmer.circuitjs.txt](rev_1/Dual Channel LED Dimmer.circuitjs.txt)
- [EasyEDA](https://easyeda.com/editor) file
    - [Dual Channel LED Dimmer.easyeda.json](rev_1/Dual Channel LED Dimmer.easyeda.json)
- Gerber files
    - [Dual Channel LED Dimmer.gerber.rar](rev_1/Dual Channel LED Dimmer.gerber.rar)

### Parts

- TODO

## Online resources

- 555/556
    - https://www.svetsoucastek.cz/media/3955/datasheet-312-004.pdf?&key=ZGpmIyQwNUZfMzk1NQ==
    - https://components101.com/sites/default/files/component_pin/NE556-Pinout.png
    - https://www.youtube.com/watch?v=i0SNb__dkYI
- Circuits
    - https://www.youtube.com/watch?v=UJhGJ3-bqS4
    - https://www.youtube.com/watch?v=R1jP2mvG0Wk
    - https://howtomechatronics.com/how-it-works/electronics/how-to-make-pwm-dc-motor-speed-controller-using-555-timer-ic/
    - https://howtomechatronics.com/wp-content/uploads/2018/02/PWM-DC-Motor-Speed-Control-with-555-Timer-IC.png
    - https://circuits-diy.com/multiple-style-flasher-using-555/
    - https://circuitdigest.com/sites/default/files/circuitdiagram/RGB-Light-Circuit-Diagram.gif

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
